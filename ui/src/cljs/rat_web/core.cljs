(ns rat-web.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [rat-web.events :as events]
            [rat-web.routes :as routes]
            [re-pollsive.core :as poll]
            [rat-web.views :as views]
            [rat-web.config :as config]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::poll/init])
  (re-frame/dispatch-sync [:initialize-db])
  (re-frame/dispatch-sync [:load-or-generate-auth-token])
  (re-frame/dispatch-sync [:load-or-generate-wallet-id])
  (re-frame/dispatch-sync [:load-or-generate-wallet-exists])
  (re-frame/dispatch-sync [:set-stage])
  (re-frame/dispatch-sync [:get-wallet])
  (re-frame/dispatch [::poll/set-rules [{:interval 20
                                         :event [:get-events]
                                         :dispatch-event-on-startup? true}]])
  (enable-console-print!)
  (dev-setup)
  (mount-root))
