(ns rat-web.events
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [cljs-crypto-rand.core :as crypto]
            [alphabase.base58 :as base58]
            [day8.re-frame.http-fx] ; registers :http-xhrio handler
            [rat-web.time]
            [rat-web.ticker :refer [max-ticker ticker->args inc-ticker ticker->str]]
            [rat-web.uuid :refer [new-id]]
            [akiroz.re-frame.storage :refer [persist-db]]
            [rat-web.db :as db]))


(js/console.log "domain: " (.-hostname (.-location js/window)))


(defn persistent-reg-event-db
  [event-id handler]
  (re-frame/reg-event-fx
    event-id
    [(persist-db :RATBank :persistent)]
    (fn [{:keys [db]} event-vec]
      {:db (handler db event-vec)})))


(defn initialize-db-handler [_ _] db/default-db)


(re-frame/reg-event-db :initialize-db initialize-db-handler)


(defn set-stage [db _]
  (let [prod (= "app.unmuni.com" (.-hostname (.-location js/window)))
        stage (if prod "prod" "test")
        api-url (if prod "https://api.unmuni.com" "https://test-api.unmuni.com")]
    (js/console.log "stage: " stage " api-url: " api-url)
    (assoc db :api-url api-url :stage stage)))


(re-frame/reg-event-db :set-stage set-stage)


; FIXME: token unused here
(defn set-active-panel-handler [db [_ active-panel token]]
   (assoc db :active-panel active-panel))

(re-frame/reg-event-db ::set-active-panel set-active-panel-handler)


(defn get-payments-handler [{db :db} _]
  (println "using token in get-payments:" (get-in db [:persistent :auth-token]))
  {:http-xhrio {:method :get
                :uri "/api/payment"
                :format (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                :on-success [:process-payment-response]
                :on-failure [:process-payment-response-failure]}
   :db (assoc db :loading? true)})

(re-frame/reg-event-fx :get-payments get-payments-handler)


(defn process-payment-response-failure-handler [db [_ result]]
  db)

(re-frame/reg-event-db :process-payment-response-failure
  process-payment-response-failure-handler)


(defn process-payment-response-handler [db [_ result]]
  (let [payments (:payments result)]
    (js/console.log (str "get-payments: downloaded " (count payments) " payments"))
    (assoc db
           :payments (:payments result)
           :your-id (:your-id result))))

(re-frame/reg-event-db :process-payment-response process-payment-response-handler)


(defn update-auth-token-handler [{db :db} [_ auth-token]]
    (js/console.log "updating auth token" auth-token)
    {:db (assoc db :auth-token auth-token)
     :dispatch [:update-auth-token-storage auth-token]})


(re-frame/reg-event-fx :update-auth-token update-auth-token-handler)


(defn update-wallet-id-handler [{db :db} [_ wallet-id]]
    (js/console.log "updating auth token" wallet-id)
    {:db (assoc db :wallet-id wallet-id)
     :dispatch [:update-wallet-id-storage wallet-id]})


(re-frame/reg-event-fx :update-wallet-id update-wallet-id-handler)


(defn update-auth-token-storage-handler [db [_ auth-token]]
  (js/console.log "updating auth token storage" auth-token)
  (assoc-in db [:persistent :auth-token] auth-token))


(persistent-reg-event-db :update-auth-token-storage update-auth-token-storage-handler)


(defn update-wallet-id-storage-handler [db [_ wallet-id]]
  (js/console.log "updating auth token storage" wallet-id)
  (assoc-in db [:persistent :wallet-id] wallet-id))


(persistent-reg-event-db :update-wallet-id-storage update-wallet-id-storage-handler)


(defn get-contact-info-handler[{db :db} _]
  (println "using token in get-payments:" (get-in db [:persistent :auth-token]))
  {:http-xhrio {:method :get
                :uri "/api/contact-info"
                :format (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                :on-success [:process-contact-info-response]
                :on-failure [:process-contact-info-response-failure]}
   :db (assoc db :loading? true)})

(re-frame/reg-event-fx :get-contact-info get-contact-info-handler)


;FIXME: payment-contacts isn't in the api any more
(defn process-contact-info-response-handler [db [_ result]]
  (js/console.log "get-contacts: downloaded" (count (:contact-info result)))
  (assoc
    db
    :contacts (:contact-info result)
    :payment-contacts (:payment-contacts result)))

(re-frame/reg-event-db :process-contact-info-response process-contact-info-response-handler)


(defn process-contact-info-response-failure-handler [db [_ result]]
  (println "get-contacts: failed" result)
  (dissoc db :contacts))

(re-frame/reg-event-db :process-contact-info-response-failure
  process-contact-info-response-failure-handler)


; FIXME: figure out how to use coeffects to generate the new-id
; FIXME: figure out how to use coeffects to generate the timestamp
(defn send-payment-handler [{db :db} [event contact value]]
  (println "in send payment")
  (let [token (new-id)
        ;FIXME: value should be an integer string now
        ;FIXME: should I be doing math here?
        payment-base {:value (.floor js/Math (* 360 (js/parseFloat value)))
                      :timestamp (rat-web.time/timestamp-now)
                      :token token
                      :unit "seconds"}
        payment (if (nil? (:person contact))
                  (assoc payment-base :contact (:id contact))
                  (assoc payment-base :recipient (:person contact)))]
    (println "payment / contact: " payment (:person contact))
    {:http-xhrio {:method :post
                  :uri "/api/payment"
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                  :params payment
                  :on-success [:process-send-payment-response token]
                  :on-failure [:process-send-payment-response-failure]}
     :db (assoc db :send-payment-loading? true :last-sent-payment payment)}))

(re-frame/reg-event-fx :send-payment send-payment-handler)


(defn get-payment-response-handler [{db :db} [event token response]]
  (println response (:ok response) (= true (:ok response)))
  {:dispatch [:get-payments]
   :db (assoc
         db
         :emailable-token token
         :payment-request-ok (= (:ok response) true)
         :error-processing-payment (:message response)
         :send-payment-loading? false)})

(re-frame/reg-event-fx :process-send-payment-response get-payment-response-handler)


(defn process-send-payment-response-failure-handler
  [db [_ result]]
  (println result)
  (js/console.log "send-payment: failed")
  (assoc db :payment-request-ok false :send-payment-loading? false))

(re-frame/reg-event-db :process-send-payment-response-failure
  process-send-payment-response-failure-handler)


(defn contact-selection-handler [db [_ choice]]
  (println "choice:" choice)
  (assoc db :recipient-is-new (= choice "__new_contact__")))

(re-frame/reg-event-db :contact-selection contact-selection-handler)


(defn add-contact-handler [{db :db} [event]]
  (let [contact {:id (new-id) :description (:contact-description db)}]
    (println "description" (:description contact))
    {:http-xhrio {:method :post
                  :uri "/api/contact-info"
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                  :params {:id (:id contact) :description (:description contact)}
                  :on-success [:process-add-contact-response contact]
                  :on-failure [:process-add-contact-response-failure]}
     :db (assoc db :add-contact-loading? true)}))

;FIXME: figure out coeffects for (new-id)
(re-frame/reg-event-fx :add-contact add-contact-handler)


(defn process-add-contact-response-handler [{db :db} [_ contact]]
  (println "contact" contact)
  (js/console.log "add-contact ok")
  {:dispatch [:get-contact-info]
   :db (assoc
         db
         :contact-to-pay contact
         :payment-step :choose-value
         :add-contact-loading? false)})


(re-frame/reg-event-fx :process-add-contact-response process-add-contact-response-handler)

(defn process-add-contact-response-failure-handler [db [_ result]]
  (println result)
  (js/console.log "add-contact: failed")
  (assoc db :contact-added-ok false :add-contact-loading? false))

(re-frame/reg-event-db :process-add-contact-response-failure
                       process-add-contact-response-failure-handler)


(defn claim-payment-handler [{db :db} [event token]]
  (println "token" token)
  {:http-xhrio {:method :post
                :uri (str "/api/claim?token=" token)
                :format (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                :on-success [:process-claim-payment-response]
                :on-failure [:process-claim-payment-response-failure]}
   :db (assoc (dissoc db :token-claimed-ok) :claim-payment-loading? true)})

(re-frame/reg-event-fx :claim-payment claim-payment-handler)


(defn process-claim-response-handler [{db :db} args]
    (println "claim-payment-response args" args)
    (js/console.log "add-contact ok")
    {:dispatch [:get-payments]
     :db (assoc db :token-claimed-ok true :claim-payment-loading? false)})

(re-frame/reg-event-fx :process-claim-payment-response process-claim-response-handler)


(defn process-claim-payment-response-failure-handler [db [_ result]]
  (println "claim payment: failed")
  (println result)
  (assoc db :token-claimed-ok false :claim-payment-loading? false))

(re-frame/reg-event-db
  :process-claim-payment-response-failure
  process-claim-payment-response-failure-handler)


(defn prefilled-claim-data-handler [db [_ data]]
  (js/console.log "prefilled claim data: " data)
  (assoc db
         :prefilled-claim-data
         (when (not (empty? data))
           (clojure.string/split data #":"))))

(re-frame/reg-event-db ::prefilled-claim-data prefilled-claim-data-handler)


(defn update-claim-data-handler [db [_ claim-data-field]]
  (assoc (dissoc db :token-claimed-ok) :claim-data-field claim-data-field))

(re-frame/reg-event-db :update-claim-data update-claim-data-handler)


(defn payment-step-change-handler [db [_ step]]
  (assoc db :payment-step step))

(re-frame/reg-event-db :payment-step payment-step-change-handler)


(defn contact-selected-handler [db [_ contact]]
  (assoc db :contact-to-pay contact :payment-step :choose-value))

(re-frame/reg-event-db :contact-selected contact-selected-handler)


(defn update-value-to-pay-handler [db [_ value]]
  (assoc db :value-to-pay value))

(re-frame/reg-event-db :update-value-to-pay update-value-to-pay-handler)


(defn reset-payments [db [_ value]]
  (assoc
    (dissoc db :contact-to-pay :value-to-pay :payment-request-ok :contact-added-ok)
    :payment-step :choose-contact :active-panel :history-panel))

(re-frame/reg-event-db :done-with-payment reset-payments)


(defn start-add-contact-handler [db args]
  (assoc db :payment-step :add-contact))

(re-frame/reg-event-db :start-add-contact start-add-contact-handler)


(defn update-contact-to-add-handler [db [_ contact-description]]
  (assoc db :contact-description contact-description))

(re-frame/reg-event-db :update-contact-to-add update-contact-to-add-handler)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; FIXME: figure out how to use coeffects to pass in the new-id
(defn load-or-generate-auth-token-handler [db _]
  (let [token (or (get-in db [:persistent :auth-token]) (new-id))]
    (js/console.log "using token" (str token))
    (assoc-in db [:persistent :auth-token] token)))

(persistent-reg-event-db :load-or-generate-auth-token load-or-generate-auth-token-handler)


(defn load-or-generate-wallet-id-handler [db _]
  (let [wallet-id (or (get-in db [:persistent :wallet-id]) (new-id))]
    (js/console.log "using wallet-id" (str wallet-id))
    (assoc-in db [:persistent :wallet-id] wallet-id)))

(persistent-reg-event-db :load-or-generate-wallet-id load-or-generate-wallet-id-handler)


(defn load-or-generate-wallet-exists [db _]
  (let [wallet-exists? (or (get-in db [:persistent :wallet-exists?]) false)]
    (js/console.log "wallet-exists?" wallet-exists?)
    (assoc-in db [:persistent :wallet-exists?] wallet-exists?)))

(persistent-reg-event-db :load-or-generate-wallet-exists load-or-generate-wallet-exists)


(defn lambda-client-get [{db :db} [event wallet-id ticker]]
  (let [url (:api-url db)
        view (if (= event :get-rollup-from-api) "wallet" "events")
        ticker-args (ticker->args ticker)]

    (println
      "in test-lambda-handler"
      " uri" (str url "/" view "/" wallet-id "?" ticker-args))

    {:http-xhrio {:method :get
                  :uri (str url "/" view "/" wallet-id "?" ticker-args)
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                  :on-success [:process-lambda-get-response]
                  :on-failure [:process-lambda-get-failure]}}))

(re-frame/reg-event-fx :get-rollup-from-api lambda-client-get)
(re-frame/reg-event-fx :get-events-from-api lambda-client-get)

(defn log-lambda-response [db [_ result]] (println db _ result) db)

(re-frame/reg-event-db :process-lambda-get-response log-lambda-response)
(re-frame/reg-event-db :process-lambda-get-failure log-lambda-response)


(defn lambda-client-put [{db :db} [event wallet-id data on-success on-error db-updates]]
  (js/console.log (str "lambda-client-put last-sent-ticker" (ticker->str (:last-sent-ticker db))))
  (js/console.log (str "lambda-client-put ticker" (ticker->str (:ticker db))))
  (js/console.log (str "lambda-client-put data: " data))
  (let [url (:api-url db)
        updated-ticker (inc-ticker (:ticker db))]
    (when (:last-sent-ticker db) (assert (>= (ticker->str (:ticker db))
                                             (ticker->str (:last-sent-ticker db)))))
    {:http-xhrio {:method :put
                  :uri (str url "/events/" wallet-id)
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :params (assoc data :ticker updated-ticker)
                  :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                  :on-success [on-success data]
                  :on-failure [on-error data]}
     :db (assoc (merge db db-updates) :last-sent-ticker updated-ticker)}))

(re-frame/reg-event-fx :register-event-in-api lambda-client-put)
(re-frame/reg-event-db :process-lambda-put-response log-lambda-response)
(re-frame/reg-event-db :process-lambda-put-failure log-lambda-response)


(defn get-wallet [{db :db} [_]]
  (let [url (:api-url db)]
    {:http-xhrio {:method :get
                  :uri (str url "/wallet/" (get-in db [:persistent :wallet-id]))
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                  :on-success [:got-wallet]
                  :on-failure [:get-wallet-failure]}}))

(re-frame/reg-event-fx :get-wallet get-wallet)


(defn got-wallet [{db :db} [_ result]]
  (js/console.log (str "wallet downloaded " result))
  {:db (assoc db :wallet result :ticker (max-ticker (:ticker db) (:ticker result)))
   :dispatch [:update-wallet-exists]})

(re-frame/reg-event-fx :got-wallet got-wallet)


(defn get-wallet-failure [db [_ result]]
  (js/console.log (str "Wallet download failed."))
  (assoc db :wallet {}))

(re-frame/reg-event-db :get-wallet-failure get-wallet-failure)


(defn update-wallet-exists [db [_ auth-token]]
  (js/console.log "updating wallet-exists?")
  (assoc-in db [:persistent :wallet-exists?] true))


(persistent-reg-event-db :update-wallet-exists update-wallet-exists)


(defn get-events [{db :db} [_]]
  (let [url (:api-url db)
        ticker (:ticker (:wallet db))]
    {:http-xhrio {:method :get
                  :uri (str url "/events/" (get-in db [:persistent :wallet-id]))
                  :params ticker
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {:x-auth-token (get-in db [:persistent :auth-token])}
                  :on-success [:got-events]
                  :on-failure [:get-events-failure]}}))

(re-frame/reg-event-fx :get-events get-events)


(defn get-events-failure [db [_ result]]
  (if (= 502 (:status result))
    (do (js/console.log "server rejected get-events request. " (str (keys result)))
        ;(js/console.log "server rejected get-events request. " (:status-text result))
        )
    (js/console.log "get-events-error " result))
  db)


(re-frame/reg-event-db :get-events-failure get-events-failure)

(defn got-events [{db :db} [_ result]]
  (js/console.log (str "events downloaded " result))
  (if (empty? result)
    {}
    {:dispatch [:get-wallet]}))

(re-frame/reg-event-fx :got-events got-events)


(defn add-contact-ok [{db :db} [_ data result]]
  (js/console.log (str "add contact ok data/result: " data result))
  {:dispatch [:get-wallet]
   :db (assoc db :contact-to-pay (:contact data)
              :payment-step :choose-value
              :add-contact-ok true
              :add-contact-loading? false)})

(re-frame/reg-event-fx :add-contact-ok add-contact-ok)

(defn add-contact-failed [db [_ data result]]
  (js/console.log (str "add contact failed " data " " result))
  (assoc db
         :add-contact-loading? false
         :contact-added-ok false))

(re-frame/reg-event-db :add-contact-failed add-contact-failed)


(defn send-payment-ok [{db :db} [_ data result]]
  (js/console.log (str "send payment ok data/result: " data result))
  {:db (assoc db :send-payment-loading? false :payment-request-ok true)
   :dispatch [:get-wallet]})

(re-frame/reg-event-fx :send-payment-ok send-payment-ok)


(defn send-payment-failed [db [_ data result]]
  (js/console.log (str "send payment failed" data result))
  (assoc db :send-payment-loading? false :payment-request-ok false))

(re-frame/reg-event-db :send-payment-failed send-payment-failed)


(defn send-claim-ok [{db :db} [_ data result]]
  (js/console.log (str "send claim ok data/result: " data (js->clj result)))
  {:db (assoc db :claim-payment-loading? false :token-claimed-ok true)
   :dispatch [:get-wallet]})

(re-frame/reg-event-fx :send-claim-ok send-claim-ok)


(defn set-contact-page [db [_ page-no]]
  (js/console.log "set contact page to " page-no)
  (assoc db :contact-page page-no))

(re-frame/reg-event-db :contact-page set-contact-page)


(defn reset-claim-form [db [_ data result]]
  (dissoc db :token-claimed-ok :prefilled-claim-data :claim-data))

(re-frame/reg-event-db :reset-claim-form reset-claim-form)
