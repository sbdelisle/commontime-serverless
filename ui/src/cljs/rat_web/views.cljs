(ns rat-web.views
  (:require [re-frame.core :as re-frame]
            [rat-web.demurrage :as dem]
            [rat-web.time :as rtime]
            [cljs-time.core :refer [now]]
            [goog.string :as gstring]
            [goog.dom :as gdom]
            [gravatar.core :refer [avatar-url]]
            [cemerick.url :refer (url url-encode)]
            [rat-web.uuid :refer [new-id]]
            [rat-web.ticker :refer [inc-ticker ticker->str]]
            [rat-web.semantic :refer [container
                                      menu
                                      menu-item
                                      statistic
                                      statistic-label
                                      statistic-value
                                      list-container
                                      list-item
                                      grid
                                      grid-column
                                      grid-row
                                      divider
                                      segment
                                      segment-group
                                      form
                                      form-input
                                      form-field
                                      form-button
                                      form-group
                                      transition-group
                                      header
                                      sub-header
                                      header-content
                                      icon
                                      image
                                      sticky
                                      button
                                      label
                                      message]]
            [rat-web.subs :as subs]))


(defn display-as-minutes [s]
  (gstring/format "%d" (/ s 60)))


(defn as-jiffies [seconds]
  (/ seconds 360))


; 10 9.0 .01 / 10 9.9 / 1.0 .99
(defn display-as-jiffies [seconds]
  (let [jiffies (as-jiffies seconds)
        magnitude (.abs js/Math jiffies)
        format-str (cond
                     (>= magnitude 9.95) "%.0f"
                     (>= magnitude 0.995) "%.1f"
                     :else "%.2f")]
    (gstring/format format-str jiffies)))


(defn display-precise-jiffies [seconds] (gstring/format "%.4f" (as-jiffies seconds)))


(defn get-value-from-input [input-id]
  (.-value (.getElementById js/document input-id)))


(defn reset-input-value [input-id]
  (gdom/setProperties (.getElementById js/document input-id) (clj->js {:value ""})))


(defn calculate-balance [payments rollup-balance]
  (let [debits (filter #(= :debit (:payment_type %)) (filter :id payments))
        credits (filter #(= :credit (:payment_type %)) (filter :id payments))
        dem-debits (map #(dem/demurrage (rtime/parse (:timestamp %)) (:value %) "seconds")
                        debits)
        dem-credits (map
                      #(dem/demurrage (rtime/parse (:timestamp %)) (:value %) "seconds")
                      credits)
        shared-now (now)]
    (js/console.log "rollup" (str (dem/quantity-at
                           (dem/from-vec (or rollup-balance [(rtime/timestamp-now) 0]))
                           shared-now)))
    (+ (dem/quantity-at (dem/from-vec (or rollup-balance [(rtime/timestamp-now) 0])) shared-now)
     (-
       (dem/quantity-at (apply dem/sum (concat [(dem/demurrage 0)] dem-credits)) shared-now)
       (dem/quantity-at (apply dem/sum (concat [(dem/demurrage 0)] dem-debits)) shared-now)))))


(defn payment-contact-description [person-id contact-list payment]
  (let
    [contacts-by-person (into {} (map #(vector (:person %) %) contact-list))
     contacts-by-id (into {} (map #(vector (:id %) %) contact-list))
     this-contact (cond
                    (= person-id (:recipient payment)) (get contacts-by-person (:sender payment))
                    (not (nil? (:recipient payment))) (get contacts-by-person (:recipient payment))
                    (not (nil? (:contact payment))) (get contacts-by-id (:contact payment))
                    :else nil)]
    (:description this-contact)))


(defn payment-value [person-id payment]
  (if
    (not= person-id (:sender payment))
    (dem/quantity-at
      (dem/demurrage (rtime/parse (:timestamp payment)) (:value payment) "seconds") (now))
    (dem/quantity-at
      (dem/demurrage (rtime/parse (:timestamp payment)) (- (:value payment))) (now))))


(defn display-minutes [minutes]
  [:span [:strong minutes] [:span {:class "ctm-symbol" :title "Jiffies"} " jiffies"]])


(def nav-buttons
  [:> grid-column {:width 12}
   [:> button
    {:size "big" :link true :href "/#/payment" :class "primary-nav" :labelPosition "left"
     :icon true}
    [:> icon {:name "arrow up"}] "Make a Payment"]

   ; [:> button
   ;  {:size "big" :link true :href "/#/claim" :class "primary-nav" :labelPosition "left"
   ;   :icon true}
   ;  [:> icon {:name "arrow down"}]  "Claim a Payment"]

   [:> button
    {:size "big" :link true :href "/#/" :class "primary-nav" :labelPosition "right"
     :icon true}
    [:> icon {:name "history"}]  "Payment History"]])


(defn balance-box []
  (let [payments (re-frame/subscribe [::subs/payments])
        rollup-balance (re-frame/subscribe [::subs/rollup-balance])
        your-id (re-frame/subscribe [::subs/your-id])
        ctm (display-as-jiffies (calculate-balance @payments @rollup-balance))]
    [:> segment {:class "unmuni-balance-box"}
     [:> grid {:padded true}
       [:> grid-column {:width 4}
        [:> statistic {:color (if (< ctm 0) "red" "green")}
         [:> statistic-value ctm]
         [:> statistic-label "Balance (jiffies)"]]]
       nav-buttons]]))


(defn display-token [payment contact]
  (if
    (nil? (:wallet contact))
    [:span {:style {:font-family "monospace"}} [:div (:sender payment)] [:div (:token payment)]]
    ""))


(defn display-timestamp [iso-timestamp]
  (let [ts (rtime/parse iso-timestamp)]
    [:span
     {:title (str (rtime/display-timestamp ts :minute) " UTC")}
     (str (rtime/display-timestamp ts :date) " UTC")]))


(defn history-panel-payment-row [payment]
  (let [contacts (re-frame/subscribe [::subs/contacts])
        contact ((keyword (:recipient payment)) @contacts)
        magnitude (:value payment)
        value (if (= :debit (:payment_type payment)) (- magnitude) magnitude)
        decayed (dem/quantity-at (dem/demurrage (rtime/parse (:timestamp payment)) value) (now))]
    ^{:key (:token payment)}
    [:> grid-row
     [:> grid-column (display-timestamp (:record_time payment))]
     [:> grid-column [:strong (if contact (:description contact) "Unknown contact")]]
     [:> grid-column
      [:span
       {:class (if (< value 0) "negative" "positive")}
       [:span {:title (display-precise-jiffies decayed)} (display-as-jiffies value)]
       " jiffies"]]
     [:> grid-column
      (let [token-markup (display-token payment contact)]
        (if (= :debit (:payment_type payment))
          (if (= "" token-markup) "Transaction complete" [:span "Unclaimed "])
          "Payment received"))]]))


(defn shared-content [& specific]
  (let [stage (re-frame/subscribe [::subs/stage])
        logo
        [:> menu {:inverted true :borderless true :fixed "top" :class "banner unmuni-banner"}
          [:> menu-item [:> header {:inverted true :textAlign "left" :class "unmuni-menu-item"}
                         [:> icon {:name "payment" :size "big" :class "unmuni-icon"}]
                         [:> header-content {:class "unmuni-header"}
                           (if (= @stage "test") "Unmuni [[[ Test ]]]" "Unmuni")
                           [:> sub-header {:class "unmuni-subheader"}
                            "Work for the common good. Money for the 99%."]]]]]
        begin-content
        [:> container {:class "main unmuni-container"}
         (balance-box)]
        content (if (empty? specific) begin-content (conj begin-content
                                                          [:> segment {:padded true}
                                                           (first specific)]))]
    [:div logo content]))


(defn history-panel []
  (let [payments (re-frame/subscribe [::subs/payments])]
    (shared-content
     [:> segment {:class "unmuni-history-panel"}
        [:> grid {:stackable true :columns 4 :container true :divided "vertically"}
        (doall
          (for [payment (reverse (sort-by :record_time (filter :id @payments)))]
            (history-panel-payment-row payment)))]])))


(defn landing-panel []
  (shared-content))


(defn select-contact [contact]
  (re-frame/dispatch [:contact-selected contact]))


(defn start-add-contact []
  (re-frame/dispatch [:start-add-contact]))


(defn display-choose-contact []
  (let [contact-page (re-frame/subscribe [::subs/contact-page])
        paged-contacts (re-frame/subscribe [::subs/paged-contacts])]
    ;(js/console.log "paged:" (str @paged-contacts))
    [:> container
     [:> segment {:padded true :class "unmuni-choose-contact"}
      [:h1 "Choose contact to pay:"]
       [:> list-container {:divided true :as "a" :relaxed true}

        [:> list-item {:on-click start-add-contact}
         [:> icon {:name "add circle" :size "big"}]
         "[ new contact ]"]

        (for [contact (if (> (count @paged-contacts) 0) (nth @paged-contacts @contact-page))]
          ^{:key (:id contact)}
          [:> list-item {:on-click #(select-contact contact)}
           [:> image {:size "mini"
                      :avatar true
                      :src (avatar-url (:description contact) :default "robohash" :https true)}]
           (:description contact)])]]

     [:> menu {:pagination true}
      (let [this-page @contact-page]
        (if (> (count @paged-contacts) 1)
          (for [page-no (range 0 (count @paged-contacts))]
            ^{:key page-no}
            [:> menu-item
             {:name (str (inc page-no))
              :on-click #(re-frame/dispatch [:contact-page page-no])
              :active (= this-page page-no)}])))]]))


(defn send-payment [wallet-id contact value-input]
  (let [timestamp (rtime/timestamp-now)
        value (.floor js/Math (* 360 (js/parseFloat value-input)))]
    (re-frame/dispatch
      [:register-event-in-api
       wallet-id
       {:action "ADD"
        :data_type "debit"
        :payment {:id (new-id)
                  :sender wallet-id
                  :recipient (:id contact)
                  :token (new-id)
                  :timestamp timestamp
                  :record_time timestamp
                  :claimed (not (nil? (:wallet contact)))
                  :value value}}
        :send-payment-ok
        :send-payment-failed
        {:send-payment-loading? true}])))


(defn update-value-to-pay []
  (re-frame/dispatch [:update-value-to-pay (get-value-from-input "payment_value")]))


(defn done-with-payment []
  (set! (.-hash window.location) "#/")
  (re-frame/dispatch [:done-with-payment]))


(defn display-choose-value []
  (let [contact-to-pay (re-frame/subscribe [::subs/contact-to-pay])
        value-to-pay (re-frame/subscribe [::subs/value-to-pay])
        value-paid (re-frame/subscribe [::subs/value-to-pay])
        last-sent-payment (re-frame/subscribe [::subs/last-sent-payment])
        send-payment-loading? (re-frame/subscribe [::subs/send-payment-loading?])
        error-processing-payment (re-frame/subscribe [::subs/error-processing-payment])
        payments (re-frame/subscribe [::subs/payments])
        rollup-balance (re-frame/subscribe [::subs/rollup-balance])
        wallet-id (re-frame/subscribe [::subs/wallet-id])
        payment-request-ok (re-frame/subscribe [::subs/payment-request-ok])
        over-balance (> @value-to-pay (as-jiffies (calculate-balance @payments @rollup-balance)))
        treasurer? (re-frame/subscribe [::subs/treasurer?])
        block-pay? (and (not @treasurer?) over-balance)]
    (console.log "treasurer? " @treasurer? "block? " block-pay? "over balance?" over-balance)
    [:> container
     [:> segment {:class "unmuni-contact-to-pay"} "Paying "
      [:strong (:description @contact-to-pay)] ":"]
     [:> segment {:class "unmuni-choose-value"}
       [:> form
        {:error (= @payment-request-ok false)
         :success (= @payment-request-ok true)
         :loading (= true @send-payment-loading?)}

        [:> form-field {:inline true :class "unmuni-amount-field"}
          [:input {:id "payment_value"
                   :label "Amount to Pay (jiffies):"
                   :disabled (or block-pay? (not (nil? @payment-request-ok)))
                   :on-change update-value-to-pay
                   :auto-complete "off"}]
          (when block-pay?
            [:> label {:basic true :pointing :left :color :red :class "unmuni-error-label"}
           "You can not pay more than your balance."])]

        [:> form-group
          [:> form-button
           {:class "unmuni-send-payment-button"
            :disabled (or block-pay? (not (nil? @payment-request-ok)))
            :on-click (when (not block-pay?)
              #(send-payment @wallet-id @contact-to-pay @value-to-pay))}
           "Pay"]
          [:> form-button {:on-click done-with-payment 
                           :disabled (not (nil? @payment-request-ok))
                           :class "unmuni-done-with-payment-button"}
           "Cancel"]]

        [:> message
         {:success true :class "unmuni-success-message"}
         "Your payment of " (display-minutes @value-paid) " to "
         [:strong (:description @contact-to-pay)] " has been recorded. "
         (if (nil? (:wallet @contact-to-pay))
           [:span
            "Unmuni has sent the recipient a token via email"
            " to complete the payment: "]
           "")
         [:> button {:on-click done-with-payment} "OK"]]

        [:> message {:error true :class "unmuni-error-message"} "Error processing payment. "
         @error-processing-payment]]]]))


(defn update-contact-to-add []
  (re-frame/dispatch [:update-contact-to-add (get-value-from-input "contact_to_add")]))


(defn add-contact [wallet-id contact-description]
  (re-frame/dispatch
    [:register-event-in-api
     wallet-id
     {:action "ADD"
      :data_type "contact"
      :contact {:id (new-id) :description contact-description}}
     :add-contact-ok
     :add-contact-failed
     {:add-contact-loading? true}]))


(defn display-add-contact []
  (let [contact-description (re-frame/subscribe [::subs/contact-description])
        contact-added-ok (re-frame/subscribe [::subs/contact-added-ok])
        wallet-id (re-frame/subscribe [::subs/wallet-id])
        add-contact-loading? (re-frame/subscribe [::subs/add-contact-loading?])]
    [:> container {:class "unmuni-display-add-contact"}
     [:> form {:error (= false @contact-added-ok)
               :loading (= true @add-contact-loading?)
               :size "huge"}

      [:h3 [:label {:for "contact_to_add"} "Email Address of contact to add:"]]
      [:> form-input {:placeholder "example@example.com"
                      :on-change update-contact-to-add
                      :class "unmuni-contact-to-add-input"
                      :id "contact_to_add"}]
      [:> form-group
        [:> form-button
         {:size "huge" :secondary true :class "unmuni-add-contact-button"
          :on-click #(add-contact @wallet-id @contact-description)}
         "Add Contact"]
       [:> form-button {:on-click done-with-payment
                        :size "huge"
                        :class "unmuni-done-with-payment-button"}
        "Cancel"]]
      [:> message {:error true :class "unmuni-error-message"} "Failed to add contact"]]]))


(defn payment-panel []
  (shared-content
   (let [payment-step (re-frame/subscribe [::subs/payment-step])]
     (println "payment-step" @payment-step)
     ((case @payment-step
       :choose-contact display-choose-contact
       :add-contact display-add-contact
       :choose-value display-choose-value)))))


(defn settings-panel []
  (shared-content
   [:div
    [:div "Authentication Token:"
      [:input {:id "auth_token_input" :type "text"}]
      [:input
       {:id "update_auth_token_button"
        :type "button"
        :value "update"
        :on-click
        #(re-frame/dispatch [:update-auth-token (get-value-from-input "auth_token_input")])}]]

    [:div "Wallet ID:"
     [:input {:id "wallet_id_input" :type "text"}]
     [:input
      {:id "update_wallet_id_button"
       :type "button"
       :value "update"
       :on-click
       #(re-frame/dispatch [:update-wallet-id (get-value-from-input "wallet_id_input")])}]]]))


(defn update-claim-data []
  (re-frame/dispatch [:update-claim-data (get-value-from-input "claim_data")]))


(defn reset-claim-form []
  (set! (.-hash window.location) "#/")
  (re-frame/dispatch [:reset-claim-form]))


(defn submit-claim [wallet-id [sender-wallet-id claim-token]]
  (let [timestamp (rtime/timestamp-now)]
    (re-frame/dispatch
      [:register-event-in-api
       wallet-id
       {:action "ADD"
        :data_type "claim"
        :claim {:sender sender-wallet-id
                :token claim-token}}
        :send-claim-ok
        :send-claim-failed
        {:claim-payment-loading? true}])))


(defn claim-panel []
  (let [wallet-id (re-frame/subscribe [::subs/wallet-id])
        claim-data (re-frame/subscribe [::subs/claim-data])
        claim-data-field (re-frame/subscribe [::subs/claim-data-field])
        prefilled-claim-data (re-frame/subscribe [::subs/prefilled-claim-data])
        claim-payment-loading? (re-frame/subscribe [::subs/claim-payment-loading?])
        claim-from-me? (re-frame/subscribe [::subs/claim-from-me?])
        claim-already-submitted? (re-frame/subscribe [::subs/claim-already-submitted?])
        invalid-claim-format? (re-frame/subscribe [::subs/invalid-claim-format?])
        token-claimed-ok (re-frame/subscribe [::subs/token-claimed-ok])]

    (js/console.log "claim" (str @claim-data))
    (js/console.log "claim from me?" (str @claim-from-me?))
    (js/console.log "claim submitted?" (str @claim-already-submitted?))
    (js/console.log "token claimed ok? " (str @token-claimed-ok))
    (shared-content
     [:> form {:success (= true @token-claimed-ok)
               :error (= false @token-claimed-ok)
               :loading (= true @claim-payment-loading?)
               :size "huge"}

      [:> form-field {:inline true :hidden true}
       [:input
        {:id "claim_data"
         :type "hidden"
         :class "unmuni-claim-token-input"
         :disabled (not (nil? @prefilled-claim-data))
         :value @claim-data-field
         :on-change update-claim-data}]]

      (cond
        (not (nil? @token-claimed-ok)) [:p]
        @claim-from-me?  [:> message {:negative true} "You can not claim your own payment."]
        @claim-already-submitted?  [:> message {:negative true}
                                    "Claim already submitted for this payment."]
        (and @invalid-claim-format? (not (empty? @claim-data))) [:> message {:negative true}
                                                                 "Invalid payment token format."]
        (not (or @claim-from-me? @claim-already-submitted? @invalid-claim-format?))
        [:> message {:positive true} "Valid payment token."])

      [:> form-button
       {:on-click #(submit-claim @wallet-id @claim-data)
        :disabled (or @claim-from-me? @claim-already-submitted? @invalid-claim-format?)
        :style  {:visibility (if @token-claimed-ok "hidden" "visible")}
        :class "unmuni-submit-claim-button"
        :secondary true
        :size "huge"} "Claim Payment"]

      [:> message {:success true :class "unmuni-success-message"}
       "The payment has been credited to your account. "
       [:> button {:on-click reset-claim-form} "OK"]]

      [:> message {:error true :class "unmuni-error-message"}
       "Error claiming payment."
       [:> button {:on-click reset-claim-form} "OK"]]])))


(defn wallet-error-panel []
  (shared-content
    (let [wallet-state (re-frame/subscribe [::subs/wallet-state])]
      (case @wallet-state
        :empty [:h1 "This device is not connected to an Unmuni wallet. "]
        :not-connected [:h1 "Connecting to the server..."]))))


(defn- panels [panel-name]
  (case panel-name
    :payment-panel [payment-panel]
    :claim-panel [claim-panel]
    :history-panel [history-panel]
    :settings-panel [settings-panel]
    :wallet-error-panel [wallet-error-panel]
    [:div]))


(defn show-panel [panel-name]
  [panels panel-name])


(defn main-panel []
  (let [wallet-state (re-frame/subscribe [::subs/wallet-state])
        active-panel (re-frame/subscribe [::subs/active-panel])]
    (js/console.log (str "wallet-state: " @wallet-state))
    (js/console.log (str "active panel: " @active-panel))
    (if
      (or (= @wallet-state :ready) (#{:claim-panel :settings-panel} @active-panel))
      [show-panel @active-panel]
      [show-panel :wallet-error-panel])))
