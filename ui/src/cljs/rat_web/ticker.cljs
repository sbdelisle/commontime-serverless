(ns rat-web.ticker
  (:require [rat-web.uuid]
            [goog.string :as gstring]
            [rat-web.time :refer [timestamp-now]]))


(defn clock-field [i]
  (let [as-str (str i)
        width 15
        zeros (take (- width (count as-str)) (repeat "0"))]
    (apply str (reverse (cons i zeros)))))


(defn idx-field [i]
  (let [as-str (str i)
        width 3
        zeros (take (- width (count as-str)) (repeat "0"))]
    (apply str (reverse (cons i zeros)))))


(defn new-ticker [clock idx safety timestamp]
 {:clock clock :timestamp (or timestamp (timestamp-now)) :idx idx :safety safety})


(defn ticker->str [{clock :clock timestamp :timestamp idx :idx safety :safety}]
  (gstring/format "%s#%s#%s#%s" (clock-field clock) timestamp (idx-field idx) safety))


(defn zero-ticker []
  (new-ticker 0 0 (rat-web.uuid/new-id) "0000-00-00T00:00:00"))


(defn ticker->hash [ticker] ticker)


(defn hash->ticker [hsh] (into {} (map #([(keyword (first %)) (second %)]) hsh)))


(defn ticker->args [ticker]
  (clojure.string/join "&" (map #(str (first %) "=" (last %)) ticker)))


(defn inc-ticker [ticker]
  (assoc ticker
         :clock (inc (:clock ticker))
         :timestamp "0000-00-00T00:00:00"
         :safety (rat-web.uuid/new-id)))


(defn max-ticker [& tickers]
  (last (sort-by ticker->str tickers)))
