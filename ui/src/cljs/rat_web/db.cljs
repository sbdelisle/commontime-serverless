(ns rat-web.db
  (:require [rat-web.ticker :refer [zero-ticker]]))


(def default-db
  {:name "Unmuni Wallet"
   :payments []
   :contacts []
   :contact-page 0
   :payment-contacts []
   :profiles []
   :wallet {}
   :ticker (zero-ticker)
   :recipient-is-new true
   :payment-step :choose-contact
   :error-processing-payment nil
   :auth-token "a-good-token"})
