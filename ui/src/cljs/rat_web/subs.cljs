(ns rat-web.subs
  (:require [re-frame.core :as re-frame :refer [subscribe]]
            [rat-web.time :as rtime]
            [cljs-time.core :refer [now]]
            [rat-web.demurrage :as dem]))


(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))


(re-frame/reg-sub
  ::auth-token
  (fn [db] (get-in db [:persistent :auth-token])))


(re-frame/reg-sub
  ::stage
  (fn [db] (:stage db)))


(re-frame/reg-sub
  ::wallet-id
  (fn [db _] (get-in db [:persistent :wallet-id])))


(re-frame/reg-sub
  ::wallet-state
  (fn [db _]
    (let [wallet (:wallet db)
          connected? (not (empty? wallet))
          exists? (get-in db [:persistent :wallet-exists?])]
      (case [connected? exists?]
        [false false] :empty
        [true false] :empty
        [false true] :not-connected
        [true true] :ready))))


(re-frame/reg-sub
  ::treasurer?
  (fn [db _] (= true (get-in db [:wallet :treasurer]))))


(re-frame/reg-sub
  ::contacts
  (fn [db]
    (get-in db [:wallet :contacts])))


(re-frame/reg-sub
  ::paged-contacts
  (fn [db]
    (partition 5 5 nil (sort-by :description (vals (get-in db [:wallet :contacts]))))))


(re-frame/reg-sub
  ::contact-page
  (fn [db]
    (:contact-page db)))


(re-frame/reg-sub
  ::payment-contacts
  (fn [db]
    (:payment-contacts db)))


(re-frame/reg-sub
  ::debits
  (fn [db]
    (concat
      (map #(assoc % :payment_type :debit) (vals (get-in db [:wallet :debits]))))))


(re-frame/reg-sub
  ::credits
  (fn [db]
    (concat
      (map #(assoc % :payment_type :credit) (vals (get-in db [:wallet :credits]))))))


(re-frame/reg-sub
  ::rollup-balance
  (fn [db]
    (get-in db [:wallet :rollup_balance])))


(re-frame/reg-sub
  ::payments
  (fn [db]
    (let [debits (subscribe [::debits])
          credits (subscribe [::credits])]
    (concat @debits @credits))))


(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))


(re-frame/reg-sub
  ::your-id
  (fn [db _]
    (:your-id db)))


(re-frame/reg-sub
  ::recipient-is-new
  (fn [db _]
    (:recipient-is-new db)))


(re-frame/reg-sub
  ::claim-data-field
  (fn [db _]
    (:claim-data-field db)))


(re-frame/reg-sub
  ::prefilled-claim-data
  (fn [db _]
    (:prefilled-claim-data db)))


(re-frame/reg-sub
  ::claim-data
  (fn [db _]
    (let [claim-data-field (subscribe [::claim-data-field])
          prefilled-claim-data (subscribe [::prefilled-claim-data])]
      (or
        @prefilled-claim-data
        (clojure.string/split @claim-data-field #":")))))


(re-frame/reg-sub
  ::claim-from-me?
  (fn [db _]
    (let [wallet-id (subscribe [::wallet-id])
          claim-data (subscribe [::claim-data])]
      (= @wallet-id (first @claim-data)))))


(re-frame/reg-sub
  ::claim-already-submitted?
  (fn [db _]
    (let [claim-data (subscribe [::claim-data])
          credits (subscribe [::credits])]
      (true? (some #(= @claim-data [(:sender %) (:token %)]) @credits)))))


(re-frame/reg-sub
  ::invalid-claim-format?
  (fn [db _]
    (let [claim-data (subscribe [::claim-data])
          bad-size? (some #(not= (count %) 22) @claim-data)
          bad-length? (not= 2 (count @claim-data))]
      (or bad-size? bad-length?))))


(re-frame/reg-sub
  ::token-claimed-ok
  (fn [db _]
    (:token-claimed-ok db)))


(re-frame/reg-sub
  ::payment-step
  (fn [db _] (:payment-step db)))


(re-frame/reg-sub
  ::contact-to-pay
  (fn [db _] (:contact-to-pay db)))


(re-frame/reg-sub
  ::contact-description
  (fn [db _] (:contact-description db)))


(re-frame/reg-sub
  ::contact-added-ok
  (fn [db _] (:contact-added-ok db)))


(re-frame/reg-sub
  ::value-to-pay
  (fn [db _] (:value-to-pay db)))

(re-frame/reg-sub
  ::payment-request-ok
  (fn [db _] (:payment-request-ok db)))

(re-frame/reg-sub
  ::error-processing-payment
  (fn [db _] (:error-processing-payment db)))

(re-frame/reg-sub
  ::last-sent-payment
  (fn [db _] (:last-sent-payment db)))

(re-frame/reg-sub
  ::add-contact-loading?
  (fn [db _] (:add-contact-loading? db)))

(re-frame/reg-sub
  ::send-payment-loading?
  (fn [db _] (:send-payment-loading? db)))

(re-frame/reg-sub
  ::claim-payment-loading?
  (fn [db _] (:claim-payment-loading? db)))

(re-frame/reg-sub
  ::clock
  (fn [db _] (:ticker db)))
