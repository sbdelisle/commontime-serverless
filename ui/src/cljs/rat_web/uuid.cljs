(ns rat-web.uuid
  (:require [cljs-crypto-rand.core :as crypto]
            [alphabase.base58 :as base58]))

(defn new-id [] (base58/encode (crypto/rand-u8 16)))
