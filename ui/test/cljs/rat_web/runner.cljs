(ns rat-web.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              ;[rat-web.core-test]
              [rat-web.demurrage-test]
              [rat-web.events-test]))

(doo-tests 'rat-web.demurrage-test
           'rat-web.events-test)
