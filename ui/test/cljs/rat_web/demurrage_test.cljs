(ns rat-web.demurrage-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [cljs-time.core :refer [date-time now plus seconds]]
            [cljs-time.core :as time-core]
            [rat-web.demurrage :as dem]))


(deftest test-internal-expt
  (testing "internally defined expt"
    (is (= (dem/expt 1 1) 1))
    (is (= (dem/expt 2 2) 4))
    (is (= (dem/expt 3 3) 27))
    (is (= (dem/expt 10 6) 1000000))))


(deftest test-internal-floor
  (testing "internally defined floor")
  (is (= (dem/floor 1.1) 1.0))
  (is (= (dem/floor 1.0) 1.0))
  (is (= (dem/floor 1.9999) 1.0)))


(deftest test-init-demurrage
  (testing "demurrage structure initializer")
  (with-redefs [now #(date-time 1986 10 14 4 3 27 456)]
    (let [new-demurrage (dem/demurrage 1337)]
      (is (time-core/= (first new-demurrage) (date-time 1986 10 14 4 3 27 456)))
      (is (= (second new-demurrage) 1337))
      (is (time-core/= (dem/timestamp new-demurrage) (date-time 1986 10 14 4 3 27 456)))
      (is (= (dem/quantity new-demurrage) 1337)))))


(deftest test-decay
  (testing "exponential decay function")
  (is (dem/decay 100 dem/lunar-month-seconds) 90)
  (is (dem/decay 100 (* 2 dem/lunar-month-seconds)) 81)
  (is (dem/decay 100 0) 100))


(deftest test-quantity-at
  (testing "decay as applied to dem structure")
  (let [a-date (date-time 1971 11 11 1 2 30 0)
        a-month-later (plus a-date (seconds dem/lunar-month-seconds))]
    (is (= (dem/quantity-at (dem/demurrage a-date 1000) a-month-later) 900))))


(deftest test-sum
  (testing "summation of demurrage structures")
  (let [a-date (date-time 1971 11 11 1 2 30 0)
        a-month-later (plus a-date (seconds dem/lunar-month-seconds))]
    (= 200 (dem/quantity (dem/sum (dem/demurrage a-date 100) (dem/demurrage a-date 100))))
    (= 181 (dem/quantity-at 
             (dem/sum (dem/demurrage a-date 100) (dem/demurrage a-month-later 100))
             a-month-later))))
