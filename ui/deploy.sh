#!/bin/bash

set -x

STAGE="$1"


if test "$STAGE" = "prod" ; then
    DISTRIBUTION_ID="E3PVLDF10L3AXR"
    S3_PATH="s3://unmuni/"
elif test "$STAGE" = "test"; then
    DISTRIBUTION_ID="E2A413OHAPW0QZ"
    S3_PATH="s3://unmuni/test/"
else
    echo "provide stage: test|prod"
    exit 1
fi

function content_addressed () {
    FILE_PATH=$1
    SUFFIX=$2
    HASH=( $(md5sum ${FILE_PATH}) )
    NEW_NAME=$(basename ${FILE_PATH} ${SUFFIX})-${HASH}${SUFFIX}

    cp $1 deploy/${NEW_NAME}
    echo ${NEW_NAME}
}

lein clean
lein cljsbuild once min

rm -rf deploy
mkdir -p deploy
cp -ax resources/public/* deploy/

APP_JS=$(content_addressed resources/public/js/compiled/app.js .js)
COM_CSS=$(content_addressed resources/public/commontime.css .css)

cat resources/public/index.html | sed "s^js/compiled/app.js^$APP_JS^g" |
    sed "s/commontime.css/$COM_CSS/g" > deploy/index.html

aws --profile personal s3 sync deploy "$S3_PATH"

aws --profile personal cloudfront create-invalidation --distribution-id "$DISTRIBUTION_ID" \
    --paths /index.html
