(def build-common
  {:target :nodejs 
   :npm-deps {:aws-sdk "2.227.1"}
   :install-deps true
   :pretty-print true})


(defproject commontime-lambda "0.0.1-SNAPSHOT"
    :plugins [[lein-cljsbuild "1.1.7"]
              [lein-doo "0.1.10"]]
    :dependencies [[org.clojure/clojurescript "1.9.946"]
                   [org.clojure/clojure "1.8.0"]
                   [cljsjs/aws-sdk-js "2.94.0-0"]
                   [org.clojure/core.async "0.4.474"]
                   [mvxcvi/alphabase "1.0.0"]
                   [com.andrewmcveigh/cljs-time "0.5.2"]
                   [com.taoensso/timbre "4.10.0"]]
    :doo {:build "test" :alias {:default [:node]}}
    :cljsbuild {
                :builds [
                  {:id "test"
                   :source-paths ["src" "test"]
                   :compiler {
                     :output-to "autotest.js"
                     :target :nodejs
                     :npm-deps {:aws-sdk "2.227.1"}
                     :install-deps true
                     :optimizations :none
                     :main commontime.test
                     :pretty-print true}}

                  {:id "local"
                   :source-paths ["src"]
                   :compiler {
                     :output-to "local.js"
                     :target :nodejs
                     :npm-deps {:aws-sdk "2.227.1"}
                     :install-deps true
                     :optimizations :none
                     :main commontime.lambda
                     :pretty-print true}}

                  {:id "lambda"
                   :source-paths ["src"]
                   :compiler {
                     :output-to "lambda.js"
                     :target :nodejs
                     :npm-deps {:aws-sdk "2.227.1"}
                     :install-deps true
                     :optimizations :simple
                     :pretty-print true}}]})
