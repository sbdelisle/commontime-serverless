(ns commontime.test
    (:require
      [taoensso.timbre :as timbre :refer [info]]
      [commontime.wallet-store :refer [rollup-transactions]]
      [commontime.lambda :refer [debit-event-new-balance]]
      [commontime.demurrage :as dem]
      [commontime.time :as cttime]
      [cljs-time.core :refer [now]]
      [cljs.test :refer-macros [deftest testing is]]
      [doo.runner :refer-macros [doo-tests]]))

; (deftest fake-test
;   (testing "fake description"
;     (is (= 1 0))))
;

(def shared-now (now))
(def shared-now-stamp (cttime/unparse shared-now))

(def wallet
  {
  :debits {
;    :a {:record_time "2018-09-18T19:16:22" :timestamp "2018-09-18T19:16:23" :value 100}
;    :b {:record_time "2018-09-18T19:16:22" :timestamp "2018-09-18T19:16:23" :value 100}
    :c {:record_time "2018-09-18T19:16:22" :timestamp shared-now-stamp :value 100}}
  :credits {
;    :d {:record_time "2018-09-18T19:16:21" :timestamp "2018-09-18T19:16:23" :value 200}
;    :e {:record_time "2018-09-18T19:16:22" :timestamp "2018-09-18T19:16:23" :value 200}
    :f {:record_time "2018-09-18T19:16:22" :timestamp shared-now-stamp :value 300}}})


(deftest test-rollup-transactions
  (testing "does it roll up?"
    (let [rolled-up (rollup-transactions wallet)]
      (is (= (second (:rollup_balance rolled-up)) 0)))))


(deftest test-debit-event-new-balance
  (testing "debit event new balance"
    (let [new-balance (debit-event-new-balance wallet {:record_time shared-now-stamp
                                                       :timestamp shared-now-stamp
                                                       :value 100})]
      (is (= (dem/quantity-at new-balance shared-now) 100)))))


(doo-tests 'commontime.test)

