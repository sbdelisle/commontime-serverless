(ns commontime.wallet-store
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [commontime.dynamo :refer [get-events-raw
                                       get-base-wallet-raw
                                       dynamo-put
                                       dynamo-get
                                       create-table
                                       expire-entries-before]]
            [commontime.demurrage :as dem]
            [commontime.time :as cttime]
            [cljs.core.async :refer [<! >! chan take!] :as async]
            [taoensso.timbre.appenders.core :as appenders]
            [commontime.dynamo-key :as dk :refer [ticker->str zero-ticker]]
            [taoensso.timbre :as timbre :refer [info]]))


(defn get-events [wallet-id ticker stage]
  (info (str "get-events wallet-id / ticker: " wallet-id ticker))
  (let [response (get-events-raw wallet-id ticker stage)
        out (chan)]
    (go
      (let [items (<! response)]
        (info "get-events items:" (str items))
        (>! out (sort-by #(ticker->str (:ticker %)) items))))
    out))


(defn get-base-wallet [wallet-id stage]
  (let [response (get-base-wallet-raw wallet-id stage)
        out (chan)]
    (go
      (let [[err {items :Items}] (<! response)]
        ; (info "get base wallet items" (str items))
        (>! out (or (first items) {:ticker (zero-ticker)}))))
    out))


(defmulti event-modify-data
  (fn [action data-type old-data data]
    (keyword (str (clojure.string/lower-case action) "-" data-type))))


(defmethod event-modify-data :del-set [action data-type old-data data]
  (disj (or old-data #{}) data))


(defmethod event-modify-data :add-set [action data-type old-data data]
  (info "in event-modify-data :add-set old-data / data" old-data "/" data)
  (conj (or old-data #{}) data))


(defmethod event-modify-data :add-scalar [action data-type old-data data]
  ; (info "in event modify data :add-scalar action/ data-type / old-data / data"
  ;      (str action data-type old-data data))
  ; (assert (= old-data nil))

  data)


(defn apply-event [wallet {data-type :data_type path :path action :action data :data
                           ticker :ticker}]
  ; (info "in apply-event: " wallet data-type path action data)
  (let [path-parts (map keyword (clojure.string/split path #"/"))
        old-data (get-in wallet path-parts)
        updated-data (event-modify-data action data-type old-data data)]
    ; (info "about to assoc-in" wallet path-parts updated-data)
    (assert (> (ticker->str ticker) (ticker->str (:ticker wallet))))
    (assoc (assoc-in wallet path-parts updated-data) :ticker ticker)))


(def cached-wallets (atom {}))


(defn rollup-wallet [wallet-id wallet ticker stage]
  (let [rollup-key (dk/->CommontimeKey wallet-id "ROLLUP" ticker "")]
    (dynamo-put
      rollup-key
      wallet
      stage)))


(defn cleanup-wallet [wallet-id ticker stage]
  (info "cleaning up wallet")
  (expire-entries-before wallet-id (max 0 (- (:clock ticker) 60)) stage))


(defn calculate-balance [debits credits]
  (let [dem-debits (map #(dem/demurrage (cttime/parse (:timestamp %)) (:value %)) debits)
        dem-credits (map #(dem/demurrage (cttime/parse (:timestamp %)) (:value %)) credits)]
    (dem/sum
     (apply dem/sum (concat [(dem/demurrage 0)] dem-credits))
     (dem/neg (apply dem/sum (concat [(dem/demurrage 0)] dem-debits))))))


(def transaction-limit 500)


(defn rollup-transactions [wallet]
  (info "in rolled up")
  (let [credits (map #(assoc % :payment_type :credit) (vals (:credits wallet)))
        debits (map #(assoc % :payment_type :debit) (vals (:debits wallet)))
        transactions (reverse (sort-by :record_time (concat credits debits)))
        keeping (take transaction-limit transactions)
        dropping (drop transaction-limit transactions)
        rollup-balance (:rollup_balance wallet)
        {kept-credits :credit kept-debits :debit} (group-by :payment_type keeping)
        {dropped-credits :credit dropped-debits :debit} (group-by :payment_type dropping)] 
    (info "rollup-balance: " rollup-balance "keeping " (count keeping)
          "dropping " (count dropping))
    (info "dem/demurrage 0 " (dem/demurrage 0))
    (info "balance of dropped " (calculate-balance dropped-debits dropped-credits))
    (info "dem/sum " (dem/sum (if (not (nil? rollup-balance))
                                          (dem/from-vec rollup-balance)
                                          (dem/demurrage 0))
                                      (calculate-balance dropped-debits dropped-credits)))
    (assoc wallet
           :rollup_balance (dem/to-vec
                             (dem/sum (if (not (nil? rollup-balance))
                                          (dem/from-vec rollup-balance)
                                          (dem/demurrage 0))
                                      (calculate-balance dropped-debits dropped-credits)))
           :credits (into {} (map #(vector (:id %) (dissoc % :payment_type)) kept-credits))
           :debits (into {} (map #(vector (:id %) (dissoc % :payment_type)) kept-debits)))))


(defn get-wallet [wallet-id stage]
  (let [out (chan)]
    (go
      (let [cache-key (str (name stage) "-" wallet-id)
            rollup-base-wallet (<! (get-base-wallet wallet-id stage))
            base-wallet (or (get @cached-wallets cache-key) rollup-base-wallet)
            got-events (<! (get-events wallet-id (:ticker base-wallet) stage))
            wallet-from-events (reduce apply-event base-wallet got-events)
            wallet (rollup-transactions wallet-from-events)]
        (swap! cached-wallets assoc cache-key wallet)
        (>! out (assoc wallet :id wallet-id))
        (info "diff from base clock"
              (- (:clock (:ticker wallet)) (:clock (:ticker rollup-base-wallet))))
        (when (>= (- (:clock (:ticker wallet)) (:clock (:ticker rollup-base-wallet))) 5)
          (rollup-wallet wallet-id wallet (:ticker wallet) stage)
          (cleanup-wallet wallet-id (:ticker wallet) stage))))
    out))


(defn add-auth-event [wallet-id ticker auth-token stage]
  (let [path "authentication"
        action "ADD"
        event-key (dk/->CommontimeKey wallet-id action ticker path)]
    (dynamo-put
      event-key
      {"path" path
       "data" auth-token
       "data_type" "set"
       "action" action
       "ticker" ticker}
      stage)))


(defn del-auth-event [wallet-id ticker auth-token stage]
  (let [path "authentication"
        action "DEL"
        event-key (dk/->CommontimeKey wallet-id action ticker path)]
    (dynamo-put
      event-key
      {"path" path
       "data" auth-token
       "data_type" "set"
       "action" action
       "ticker" ticker}
      stage)))


(defn add-contact-event [wallet-id ticker contact stage]
  (info "add-contact-event" wallet-id (ticker->str ticker) contact)
  (let [path (str "contacts/" (:id contact))
        action "ADD"
        event-key (dk/->CommontimeKey wallet-id action ticker path)]
    (dynamo-put
      event-key
      {"path" path
       "data" contact
       "data_type" "scalar"
       "action" action
       "ticker" ticker}
      stage)))


(defn del-contact-event [wallet-id ticker contact stage]
  (let [path (str "contacts/" (:id contact))
        action "DEL"
        event-key (dk/->CommontimeKey wallet-id action ticker path)]
    (dynamo-put
      event-key
      {"path" path
       "data" nil
       "data_type" "scalar"
       "action" action
       "ticker" ticker}
      stage)))


(defn debit-event [wallet ticker payment stage]
  ; (info "debit-event, wallet: " wallet)
  (let [path (str "debits/" (:id payment))
        action "ADD"
        event-key (dk/->CommontimeKey (:id wallet) action ticker path)]
    (assert (not (contains? (:debits wallet) (:id payment))))
    (dynamo-put
      event-key
      {"path" path
       "data" payment
       "data_type" "scalar"
       "action" action
       "ticker" ticker}
      stage)))


(defn credit-event [wallet ticker payment stage]
  (let [path (str "credits/" (:id payment))
        action "ADD"
        event-key (dk/->CommontimeKey (:id wallet) action ticker path)]
    (assert (not (contains? (:credits wallet) (:id payment))))
    (dynamo-put
      event-key
      {"path" path
       "data" payment
       "data_type" "scalar"
       "action" action
       "ticker" ticker}
      stage)))


(defn notify-claimed-event [sender-wallet ticker payment stage]
  (let [payment-id (:id payment)
        path (str "debits/" payment-id "/claimed")
        action "ADD"
        event-key (dk/->CommontimeKey (:id sender-wallet) action ticker path)]
    (assert (= false (get-in sender-wallet [:debits payment-id :claimed])))
    (dynamo-put
      event-key
      {"path" path
       "data" true
       "data_type" "scalar"
       "action" action
       "ticker" ticker}
      stage)))


(defn contact-verified-event [sender-wallet ticker contact-id recipient-wallet-id stage]
  (let [path (str "contacts/" contact-id "/wallet")
        action "ADD"
        event-key (dk/->CommontimeKey (:id sender-wallet) action ticker path)]
    (assert (nil? (get-in sender-wallet [:contacts (keyword contact-id) :wallet])))
    (dynamo-put
      event-key
      {"path" path
       "data" recipient-wallet-id
       "data_type" "scalar"
       "action" action
       "ticker" ticker}
      stage)))


(defn log-result [msg]
  (fn [e data] (info msg e data)))


(defn test-events []
  (take! (create-table "prod") (log-result "create-table")))
  ; (take! (add-auth-event "george" 1234 "xyz-123" "my-secret-token") (log-result "add-auth")))
  ; (take! (del-auth-event "george" 1235 "abc-123" "my-secret-token") (log-result "del auth")))
  ; (take! (get-base-wallet "george") (log-result "get-base-wallet")))
  ; (take! (get-events "george" 1230) (log-result "get-events-raw")))
  ; (take! (get-wallet "george") (log-result "get-wallet")))
  ; (take! (debit-event "george" 1236 "111x22" {"recipient" "betty" "id" "x121y121"}) (log-result "debit-event ")))
  ; (take! (get-events-raw "george" 1230) (log-result "get-events-raw")))
