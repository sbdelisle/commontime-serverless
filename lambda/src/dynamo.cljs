(ns commontime.dynamo
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [taoensso.timbre.appenders.core :as appenders]
            [taoensso.timbre :as timbre :refer [info]]
            [commontime.dynamo-key :as dk :refer [ticker->str ticker->search clock-field]]
            [cljs.core.async :refer [<! >! chan take!] :as async]))


(info "Loading commontime.dynamo.")


(def aws-sdk (js/require "aws-sdk"))


(def tables {:prod "unmuni" :test "unmuni-test"})


(defn set-config []
  (let [config (.-config aws-sdk)]
    (info "Setting Config.")
    (.update config (clj->js {
      "region" "us-west-2"
      "apiVersions" "2012-08-10"}))))


(set-config)


(defn get-doc-client []
  (let [DocumentClient (aget aws-sdk "DynamoDB" "DocumentClient") ]
    (DocumentClient.)))


(defn get-client []
 (let [DynamoDB (aget aws-sdk "DynamoDB")]
   (DynamoDB.)))


(defn mk-handle-dyn []
  (let [out (chan)]
    [(fn [err data]
       (when err (info "handle-dyn err: " (str err)))
       ; (info "mk-handle-dyn data: " (str (js->clj data :keywordize-keys true)))
       (go (>! out [err (js->clj data :keywordize-keys true)])))
     out]))


(defn dynamo-schema [stage]
  {:TableName (stage tables)
   :KeySchema
   [{:AttributeName "hash_key" :KeyType "HASH"}
     {:AttributeName "range_key":KeyType "RANGE"}]

   :AttributeDefinitions
   [{:AttributeName "hash_key" :AttributeType "S"}
    {:AttributeName "range_key" :AttributeType "S"}]

   :ProvisionedThroughput {:ReadCapacityUnits 5 :WriteCapacityUnits 5}})


(defn create-table [stage]
  (let [client (get-client)
        [handle-dyn, out] (mk-handle-dyn)]
    (.createTable client (clj->js (dynamo-schema stage)) handle-dyn)
    out))


(defn dynamo-get [commontime-key stage]
  (let [client (get-doc-client)
        [handle-dyn, out] (mk-handle-dyn)
        js-key (clj->js (dk/->hash commontime-key))]
    (.get client #js {:TableName (stage tables) :Key js-key} handle-dyn)
    out))


(defn dynamo-put [commontime-key clj-data stage]
  (let [client (get-doc-client)
        data (clj->js (merge (dk/->hash commontime-key) clj-data))
        [handle-dyn out] (mk-handle-dyn)]
    (info "dynamo-put, data: " (js->clj data :keywordize-keys true))
    (.put client #js {"TableName" (stage tables) "Item" data} handle-dyn)
    out))


(defn get-put-events-raw [wallet-id ticker stage]
  (let [client (get-doc-client)
        [handle-dyn out] (mk-handle-dyn)
        cutoff (str "ADD#" (ticker->search ticker))]

    (info "get-put-events ticker search" ticker (ticker->search ticker))
    (info "restricting events query to >= " cutoff "")
    (info "ExpressionAttributeValues " {":hkey" wallet-id ":rkey" cutoff ":bound" "ADD$"})
    (.query
      client
      #js {
       "TableName" (stage tables)
       "KeyConditionExpression" "hash_key = :hkey AND range_key BETWEEN :rkey AND :bound"
       "ExpressionAttributeValues" #js {":hkey" wallet-id ":rkey" cutoff ":bound" "ADD$"}}
      handle-dyn)
    out))


(defn get-del-events-raw [wallet-id ticker stage]
  (let [client (get-doc-client)
        [handle-dyn out] (mk-handle-dyn)
        cutoff (str "DEL#" (ticker->search ticker))]
    (info "get-del-events ticker search" (ticker->search ticker))
    (info "restricting events query to >= " cutoff)
    (.query
      client
      #js {
       "TableName" (stage tables)
       "KeyConditionExpression" "hash_key = :hkey AND range_key BETWEEN :rkey AND :bound"
       "ExpressionAttributeValues" #js {":hkey" wallet-id ":rkey" cutoff ":bound" "DEL$"}}
      handle-dyn)
    out))


(defn get-events-raw [wallet-id ticker stage]
  (let [out (chan)]
    (go
      (let [[pur-err put-response] (<! (get-put-events-raw wallet-id ticker stage))
            [del-err del-response] (<! (get-del-events-raw wallet-id ticker stage))]
      (>! out (concat (:Items put-response) (:Items del-response)))))
    out))


(defn get-base-wallet-raw [wallet stage]
  (let [client (get-doc-client)
       [handle-dyn out] (mk-handle-dyn)]
    (.query
      client
      #js {
        "TableName" (stage tables)
        "Limit" 1
        "ScanIndexForward" false
        "KeyConditionExpression" "hash_key = :hkey AND range_key >= :rkey"
        "ExpressionAttributeValues" #js {":hkey" wallet ":rkey" "ROLLUP#"}}
      handle-dyn)
    out))


(defn entries-to-expire [wallet-id event-type clock stage]
  (let [client (get-doc-client)
        [handle-dyn out] (mk-handle-dyn)
        base (str event-type "#")
        cutoff (str event-type "#" (clock-field clock))]
    (.query
      client
      #js {
       "TableName" (stage tables)
       "KeyConditionExpression" "hash_key = :hkey AND range_key BETWEEN :base AND :cutoff"
       "ExpressionAttributeValues" #js {":hkey" wallet-id ":base" base ":cutoff" cutoff}}
      handle-dyn)
    out))


(defn log-delete-result [c]
  (go
    (let [result (<! c)]
      (info "delete result " result))))


(defn delete-entry [hash-key range-key stage]
  (let [client (get-doc-client)
        [handle-dyn out] (mk-handle-dyn)]
    (info "deleting: " hash-key " " range-key)
    (.delete
     client
     #js
     {"TableName" (stage tables)
      "Key" #js {"hash_key" hash-key "range_key" range-key}}
     handle-dyn)
    (log-delete-result out)))



(defn expire-entries-before [wallet-id clock stage]
  (let [channels (map #(entries-to-expire wallet-id % clock stage) ["DEL" "ADD" "ROLLUP"])
        delete-fn (fn [{range-key :range_key}] (delete-entry wallet-id range-key stage))]
    (doseq [c channels]
      (go
        (let [[error result] (<! c)
              events (get result :Items)]
          (info (count events) " events to delete for wallet " wallet-id)
          (doall (map delete-fn events)))))))
