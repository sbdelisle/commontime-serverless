(ns commontime.uuid
  (:require 
    [cljs.core.async :refer [<! >! chan take!] :as async]  
    [alphabase.base58 :as base58]))


(def crypto (js/require "crypto"))


(defn new-id [] (base58/encode (.randomBytes crypto 16)))


(defn zero-id [] (base58/encode (js/Buffer. 16)))
