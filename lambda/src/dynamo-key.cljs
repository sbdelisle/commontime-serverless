(ns commontime.dynamo-key
  (:require [cljs-time.format :as f]
            [cljs-time.core :refer [now]]  
            [commontime.uuid]
            [taoensso.timbre :as timbre :refer [info]]))


(def util (js/require "util"))

(def format (.-format util))

(defn timestamp-now [] (f/unparse (f/formatters :date-hour-minute-second) (now)))


(defn clock-field [i]
  (let [as-str (str i)
        width 15
        zeros (take (- width (count as-str)) (repeat "0"))]
    (apply str (reverse (cons i zeros)))))


(defn idx-field [i]
  (let [as-str (str i)
        width 3
        zeros (take (- width (count as-str)) (repeat "0"))]
    (apply str (reverse (cons i zeros)))))


(defn inc-ticker [ticker]
  (assoc ticker :clock (inc (:clock ticker)) :safety (commontime.uuid/zero-id)))


(defn server-touch [ticker timestamp idx]
  (assoc ticker  :timestamp timestamp :idx idx))


(defn new-ticker [clock idx safety timestamp]
  {:clock clock :timestamp (or timestamp (timestamp-now)) :idx idx :safety safety})


(defn zero-ticker []
  (new-ticker 0 0 (commontime.uuid/zero-id) "0000-00-00T00:00:00"))


(defn ticker->str [{clock :clock timestamp :timestamp idx :idx safety :safety}]
  (format "%s#%s#%s#%s" (clock-field clock) timestamp (idx-field idx) safety))


(defn ticker->search [{clock :clock timestamp :timestamp idx :idx}]
  (format "%s#%s#%s$" (clock-field clock) timestamp (idx-field idx)))


(defn ticker->hash [ticker] ticker)


(defn hash->ticker [hsh]
  (let [relevant (select-keys hsh [:clock :timestamp :idx :safety])]
    (into {} (map #(vector (keyword (first %)) (second %)) relevant))))


(defprotocol DynamoKey
  (->hash [this])

  (sort-part [this])

  (->str [this]))


(defrecord CommontimeKey [wallet action ticker path]
  DynamoKey

  (->hash [this] {"hash_key" wallet
                  "range_key" (sort-part this)})

  (sort-part [this]
    (format "%s#%s#%s" action (ticker->str ticker) path))

  (->str [this]
    (format (str "hash: " wallet ", sort: " (sort-part this)))))
