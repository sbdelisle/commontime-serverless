(ns commontime.email
  (:require-macros [cljs.core.async.macros :refer [go]]) 
  (:require [taoensso.timbre :as timbre :refer [info]]
            [goog.string :as gstring]))


(def postmark (js/require "postmark"))
(def Client (aget postmark "Client"))


(def postmark-token (or (.-POSTMARK_TOKEN (.-env js/process)) "POSTMARK_API_TEST"))
(info "Using POSTMARK Token" postmark-token)

(def client (Client. postmark-token))


(defn send-email-via-postmark [{recipient :recipient subject :subject text :text}]
  (.sendEmail client
    (clj->js {:From "Unmuni Notification <support@unmuni.com>"
              :To recipient
              :Subject subject 
              :TextBody text})))


(defn display-as-jiffies [seconds]
  (let [jiffies (float (/ seconds 360))
        magnitude (.abs js/Math jiffies)
        format-str (cond
                     (>= magnitude 9.95) "%.0f"
                     (>= magnitude 0.995) "%.1f"
                     :else "%.2f")]
    (gstring/format format-str jiffies)))


(def site-url {:test "https://test.unmuni.com" :prod "https://app.unmuni.com"})


(defn send-invite-contents [payment contact stage]
  (let [display-value (display-as-jiffies (:value payment))]
    {:recipient contact
     :subject (str "Unmuni payment of " display-value " jiffies")
     :text (str "An Unmuni user has sent you a payment of " display-value
                " jiffies at " (:timestamp payment) ". "
                "You can claim your payment by visiting the following url:\n\n"
                "\t\t" (stage site-url)  "/#/claim/" (:sender payment) ":"
                (:token payment))}))


(defn send-invite! [payment contact stage]
  (let [contents (send-invite-contents payment contact stage)]
    (info "Sending invite email to " contact)
    (info "Postmark response: " (send-email-via-postmark contents))))
