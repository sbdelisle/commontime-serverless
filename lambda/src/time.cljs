(ns commontime.time
  (:require [cljs-time.format :as f]
            [taoensso.timbre :as timbre :refer [info]]
            [cljs-time.core :refer [now days ago]]))

(def rat-format (f/formatters :date-hour-minute-second))

(def rat-display-format (f/formatter "yyyy-MM-dd HH:mm"))

(def rat-date-display-format (f/formatter "d MMMM yyyy"))


(defn unparse [s]
  (f/unparse rat-format s))


(defn parse [s]
  (f/parse rat-format s))


(defn timestamp-now []
 (f/unparse rat-format (now)))


(defn timestamp-90d []
  (f/unparse rat-format (ago (days 90))))


(defn display-timestamp [t resolution]
  (case resolution
    :date (f/unparse rat-date-display-format t)
    (f/unparse rat-display-format t)))
