(ns commontime.lambda
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [taoensso.timbre :as timbre :refer [info]]
            [clojure.string]
            [cljs.core.async :refer [<! >! chan take!] :as async]
            [commontime.demurrage :as dem]
            [cljs-time.core :refer [now]]
            [commontime.time :as ctime]
            [commontime.email :as email]
            [commontime.dynamo-key :refer [timestamp-now
                                           ticker->str
                                           hash->ticker
                                           server-touch]]
            [commontime.dynamo :refer [create-table]]
            [commontime.wallet-store :as ws :refer [calculate-balance
                                                    get-wallet
                                                    get-events
                                                    add-auth-event
                                                    del-auth-event
                                                    add-contact-event
                                                    del-contact-event
                                                    credit-event
                                                    contact-verified-event
                                                    notify-claimed-event]]))


(def treasurer-auth-token (.-TREASURER_AUTH_TOKEN (.-env js/process)))


(def treasurer-wallet-id (.-TREASURER_WALLET_ID (.-env js/process)))


(defn is-treasurer? [request-auth-token wallet-id]
  (info "verifying provided auth: " wallet-id "/" request-auth-token " vs. "
        "treasurer auth:" treasurer-wallet-id "/" treasurer-auth-token ".")
  (and (= wallet-id treasurer-wallet-id) (= request-auth-token treasurer-auth-token)))


(defn verify-auth [request-auth-token wallet]
  (let [wallet-auth-tokens (:authentication wallet)]
    (info "verifying provided auth token: " request-auth-token " vs.: " wallet-auth-tokens)
    (some #(= request-auth-token %) wallet-auth-tokens)))


(defn lambda-proxy-response [headers body]
  #js {"isBase64Encoded"  false
       "statusCode" 200,
       "headers" (clj->js headers)
       "body" body})


(def reply-headers
  #js {"access-control-allow-origin"
       "*"

       "access-control-allow-headers"
       "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token"

       "access-control-allow-methods"
       "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT"})


(defn commontime-callback [aws-callback data]
  (aws-callback nil (lambda-proxy-response reply-headers data)))


(defn handle-get-wallet [wallet-id auth-token aws-callback stage]
  (go
    (let [wallet (<! (get-wallet wallet-id stage))
          wallet-with-note (if (is-treasurer? auth-token wallet-id)
                             (assoc wallet :treasurer true)
                             wallet)
          authenticated (or (is-treasurer? auth-token wallet-id) (verify-auth auth-token wallet))]
      (info "called (handle-get-wallet " wallet-id auth-token  " <callback>).")
      (info "is treasurer? " (is-treasurer? auth-token wallet-id))
      (assert authenticated)
      (commontime-callback
        aws-callback
        (.stringify js/JSON (clj->js wallet-with-note))))))


(defn handle-get-events [wallet-id auth-token params aws-callback stage]
  (go
    (let
      [wallet (<! (get-wallet wallet-id stage))
       authenticated (or (is-treasurer? auth-token wallet-id) (verify-auth auth-token wallet))]
      (assert authenticated)
      (commontime-callback
        aws-callback
        (.stringify js/JSON (clj->js (<! (get-events wallet-id (hash->ticker params) stage))))))))


(defn handle-get-url [base-path wallet-id auth-token params callback stage]
  (info "in handle-get-url" "base-path: " base-path "wallet-id: " wallet-id)
  (assert (not (nil? base-path)))
  (case base-path
    "events" (handle-get-events wallet-id auth-token params callback stage)
    "wallet" (handle-get-wallet wallet-id auth-token callback stage)))


(defn claim-event [recipient-wallet auth-token ticker claim stage]
  (info "claim-event recipient ticker uuid claim" recipient-wallet ticker claim)
  (go
    (let [sender-wallet (<! (get-wallet (:sender claim) stage))
          sender-ticker (:ticker sender-wallet)
          sender-debits (vals (:debits sender-wallet))
          payment (first (filter #(= (:token claim) (:token %)) sender-debits))
          unverified (nil?
                       (get-in sender-wallet [:contacts (keyword (:recipient payment)) :wallet]))
          event-time (timestamp-now)]
      (info "resolved wallet" sender-wallet)
      (info "resolved sender-debits" sender-debits)
      (info "resolved payment" payment)
      [(add-auth-event (:id recipient-wallet) ticker auth-token stage)
       (credit-event recipient-wallet (server-touch ticker event-time 1) payment stage)
       (notify-claimed-event sender-wallet (server-touch sender-ticker event-time 0) payment stage)
       (if unverified
         (contact-verified-event sender-wallet
                                 (server-touch sender-ticker event-time 1)
                                 (:recipient payment)
                                 (:id recipient-wallet)
                                 stage))])))


(defn get-rollup-balance [wallet]
  (let [rollup-balance (:rollup_balance wallet) ]
    (dem/sum (if (not (nil? rollup-balance))
               (dem/from-vec rollup-balance)
               (dem/demurrage 0)))))


(defn debit-event-new-balance [wallet payment]
  (let [balance-since-rollup (calculate-balance (conj (vals (:debits wallet)) payment)
                                                (vals (:credits wallet)))
        rollup-balance (get-rollup-balance wallet)]
    (dem/sum rollup-balance balance-since-rollup)))


(defn debit-event [wallet ticker payment stage]
  (go
    (let [new-balance (dem/quantity-at (debit-event-new-balance wallet payment) (now))
          contact ((keyword (:recipient payment)) (:contacts wallet))
          recipient-email (:description contact)]
      (info "in debit-event")
      (info "contacts " (:contacts wallet))
      (info "contact id " (:recipient payment))
      (info "contact " contact)
      (info "recipient email" recipient-email)

      (assert (and (> (:value payment) 0)
                   (or (= (:id wallet) treasurer-wallet-id) (> new-balance 0))
                   (= (:id wallet) (:sender payment))
                   (not= (:id wallet) (:recipient payment))))
      (let [result (ws/debit-event wallet ticker payment stage)]
        (if (nil? (:wallet contact))
          (email/send-invite! payment recipient-email stage)
          (let [recipient-wallet (<! (get-wallet (:wallet contact) stage))
                event-time (timestamp-now)]
            (credit-event recipient-wallet (server-touch ticker event-time 1) payment stage)))
        {:ok true}))))


(defn dispatch-put [wallet-id auth-token data stage]
  (let [ticker (:ticker data)]
    (go
      (let [wallet (<! (get-wallet wallet-id stage))
            authenticated (or (is-treasurer? auth-token wallet-id)
                              (verify-auth auth-token wallet))]
        (assert (> (ticker->str ticker) (ticker->str (hash->ticker (:ticker wallet)))))
        (info "dispatching for action/data-type" (:action data) (:data_type data))

        (if authenticated
         (<! (case [(:action data) (:data_type data)]
               ["ADD" "auth"] (add-auth-event wallet-id ticker (:auth-token data) stage)
               ["DEL" "auth"] (del-auth-event wallet-id ticker (:auth-token data) stage)
               ["ADD" "contact"] (add-contact-event wallet-id ticker (:contact data) stage)
               ["DEL" "contact"] (del-contact-event wallet-id ticker (:contact data) stage)
               ["ADD" "debit"] (debit-event wallet ticker (:payment data) stage)
               ["ADD" "claim"] (claim-event wallet auth-token ticker (:claim data) stage)))
         (<! (case [(:action data) (:data_type data)]
               ["ADD" "claim"] (claim-event wallet auth-token ticker (:claim data) stage))))))))


(defn handle-put-url [base-path wallet-id auth-token data aws-callback stage]
  (info "in handle-put-url" "base-path: " base-path "wallet-id: " wallet-id)
  (assert (= "events" base-path))
  (go
    (commontime-callback
      aws-callback
      (.stringify
        js/JSON
        (clj->js (<! (dispatch-put wallet-id auth-token data stage)))))))


(defn load-json-body [lambda-event]
  (js->clj (.parse js/JSON (.-body lambda-event)) :keywordize-keys true))


(defn lambda-http-handler [lambda-event context callback]
  (let [path (.-path lambda-event)
        params (js->clj (.-queryStringParameters lambda-event) :keywordize-keys true)
        [_ base-path wallet-id] (clojure.string/split path #"/")
        headers (js->clj (.-headers lambda-event))
        auth-token (get headers "x-auth-token")
        stage (keyword (.-stage (.-requestContext lambda-event)))
        method (.-httpMethod lambda-event)
        ctx stage]
    (info "lambda-handler base-path, wallet-id, params" base-path wallet-id params)
    (info "stage: " stage)
    (case method
      "GET" (handle-get-url base-path wallet-id auth-token params callback stage)
      "PUT" (handle-put-url base-path wallet-id auth-token
                            (load-json-body lambda-event) callback stage))))


(defn lambda-cron-handler [lambda-event context callback]
  (let [stage (.-stage lambda-event)
        action (.-action lambda-event)]
    (when (= action "clean-up")
      (info "clean up: unimplemented"))))


(defn lambda-handler [lambda-event context callback]
  (if (= "true" (.-cron lambda-event))
    (lambda-cron-handler lambda-event context callback)
    (lambda-http-handler lambda-event context callback)))



(set! (.-exports js/module) #js {:handler lambda-handler})


(defn noop [& args]
  (info "Don't execute anything during module import on AWS Lambda."))


(defn -main [& args]
 (create-table :prod)
 (create-table :test)
 (info "in main"))


(set! *main-cli-fn*
      (if (= "true" (.-RUN_LOCAL (.-env js/process))) -main noop))
